<?php

require_once($_SERVER['DOCUMENT_ROOT']. '/api/classes/order.php');
require_once($_SERVER['DOCUMENT_ROOT']. '/api/classes/cctranshist.php');

class PaymentCtrl extends Payment{
	/*
     *   Constructor
     */
    public function __construct($data, $paymentTy, $folder) {
        parent::__construct($data, $paymentTy, $folder);

        $this->data['pubKey'] = $this->execPaymentTyValue_get(2);
        $this->data['dataName'] = $this->execPaymentTyValue_get(3);
        $this->data['dataDescription'] = $this->execPaymentTyValue_get(4);
        $this->data['cards'] = array();

        $customerIdRs = $this->execStripeCustomerRetrieve();
        if (ExecuteDB::existRecord($customerIdRs)) {
            if (($customerIdRs[0]["result"] == 0) && !empty($customerIdRs[0]["customerId"])) {
                $this->data['cards'] = $customerIdRs;

                if (!isset($this->data['idPaymentTyId']) && !empty($customerIdRs["idPaymentTyId"])) {
                    $this->data['idPaymentTyId'] = $customerIdRs["idPaymentTyId"];
                }
            }
        }
    }

	public function paymentForm_get($app) {
		$this->paymentCommonForm_get();

		$idPaymentTyResult = $this->execPaymentTy_withdrawal_get($this->data['id'], 1);
		if (ExecuteDB::existRecord($idPaymentTyResult)){
			$this->data['formElements'] = $this->createPaymentFormElement($idPaymentTyResult, true);
			$this->data['idPaymentTyId'] = $idPaymentTyResult[0]['IDPaymentTyID'];
		}
		else {
			$this->data['formElements'] = array();
			$this->data['idPaymentTyId'] = -1;
		}

		if (isset($this->data['total'])) {
			$this->data['totalCents'] = $this->data['total'] * 100;
		}
		else {
			$this->data['totalCents'] = 0;
		}

      //$this->paymentData_set();

		// currency code set for checkout.js item
		$resultCurrencyRec =  execCurrencyTyDetail_get($this->data['currencyTy']);
		$this->data['currencyCode'] = $resultCurrencyRec['CurrencyCode'];
		// data-name set for checkout.js item
		$this->data['companyName'] = execSysPref_get_old('CP', $this->data['language'], $pid=0, "1228");

      // Update the templates path
      $app->config('templates.path', ROOT_DIR . '/routes/payments/' . $this->folder);

      // Return payment form
      echo $app->render('paymentForm.html', $this->data);
    }

    public function paymentUpdate_post($app) {

        $isIdPaymentPutSucceed = false;
        $errorMsg = array();
        $errorCode = array();
        $isApproved = false;
        $serviceFeeUpdValue = $this->data['serviceFee'];

		if (isset($this->data['customerId'])) {
			$isIdPaymentPutSucceed = true;
		}
		else if (isset($this->data['stripeToken'])) {
			$customerIdRs = $this->execStripeCustomerCreate();

			if ($customerIdRs) {
				if (($customerIdRs["result"] == 0) && !empty($customerIdRs["customerId"])) {
					$isIdPaymentPutSucceed = true;
					$this->data['customerId'] = $customerIdRs["customerId"];

					if (!isset($this->data['idPaymentTyId']) && !empty($customerIdRs["idPaymentTyId"])) {
						$this->data['idPaymentTyId'] = $customerIdRs["idPaymentTyId"];
					}
				}
				else {
					$errorCode[] = $customerIdRs['result'];
					$errorMsg[] = $customerIdRs['message'];
				}
			}
			else {
				$errorMsg[] = '[C] Unknown error occurred while saving the card.';
			}
		} //isset($this->data['stripeToken'])
		else {
			$errorMsg[] = '[D] no token';
		}

		if (isset($this->data['customerId']) && $isIdPaymentPutSucceed) {
			$stripeResult = $this->execStripeCharge();

			$order = new Order($this->data);
			if ($stripeResult[1] == 0) {
				$isApproved = true;
				if ($this->isSignUpProcess($this->data['transUrl'])) {
					execSignup_Final($this->data['id']);
				}
            $order->execOrder_Paid(0);
			}
			else {
				$errorCode[] = $stripeResult[1];
				$errorMsg[] = '[G] '.$stripeResult[2];
				$order->execOrder_confirm();
			}
		} //(!empty($this->customer) && $isIdPaymentPutSucceed
		else {
			$errorMsg[] = '[E] customer invalid';
		}

        $result = $this ->returnPaymentTransactionResult($isApproved, $errorMsg, $errorCode);
        $result['idPaymentTyId'] = isset($this->data['idPaymentTyId']) ? $this->data['idPaymentTyId'] : 0;

		return $result;
    }

    public function paymentResult_get($app) {
        $this->data['webTextItems'] = retrieveWebTextAll('/shopping/payment_complete.asp', $this->data['language'], 0);

        $this->paymentCommonResult_get();
        // Update the templates path
        $app->config('templates.path', ROOT_DIR . '/routes/payments/' . $this->folder);

        echo $app->render('paymentResult.html', $this->data);
    }

    public function idPaymentTyAddForm_get($app) {
		// retrieve PaymentValueParam records (paramValue is empty) for adding payment type
		$idPaymentTyResult = $this->execPaymentTy_withdrawal_get($id=0, 1);

		$formElements = array();
		if (ExecuteDB::existRecord($idPaymentTyResult)){
			$formElements = $this->createPaymentFormElement($idPaymentTyResult, true);
			$idPaymentTyId = "0";
		}
		else {
			$idPaymentTyId = -1;
		}

		// remove credit card number item from formElements for not displaying credit card number input item
		foreach ($formElements as $keyElm => $rowElm ) {
			if ($rowElm["fieldName"] == "1163"){ // Customer token is not editable
				unset($formElements[$keyElm]);
			}
		}

        $this->paymentData_set();

        $this->data['formElements'] = $formElements;
        $this->data['idPaymentTyId'] = $idPaymentTyId;

        // set input validation data
        $this->data['validationPaymentRule'] = $this->setValidateData();

        // set for setupCountryStateSelectors js function
        if (checkHTTPS()==true) $this->data['serveraName'] = getUrl();

        // Update the templates path
        //$app->config('templates.path', ROOT_DIR . '/routes/payments/' . $this->folder);
        $app->config('templates.path', ROOT_DIR . '/routes/payments/no-data-required');

        // Return payment form
        echo $app->render('idPaymentTyAddForm.html', $this->data);
	}

	public function idPaymentTyEditForm_get($app) {
		$idPaymentTyResult = $this->execIDPaymentTyDetail_get($this->data['idPaymentTyId'], 1);

		if (ExecuteDB::existRecord($idPaymentTyResult)){
			$this->data['idPaymentTyId'] = $this->data['idPaymentTyId'];
			foreach($this->data['cards'] as $cardIndex => $card) {
				$this->data['cards'][$cardIndex]['formElements'] = $this->createPaymentFormElement($idPaymentTyResult, true, $card);
			}
		}
		else {
			$this->data['idPaymentTyId'] = -1;
		}
		$this->data['cardAdd']['formElements'] = $this->createPaymentFormElement($idPaymentTyResult, true);

		$this->paymentData_set();

		// get chargeable value
		$resultCharge = $this->execIDPaymentTy_get_byIDPaymentTyID($this->data['idPaymentTyId']);
		$defaultCheck = "";
		if (ExecuteDB::existRecord($resultCharge)){
			if ($resultCharge['Chargeable']=="Y") {
				$defaultCheck  = 'checked';
			}
		}
		$this->data['defaultCheck'] = $defaultCheck;

		// set input validation data
		$this->data['validationPaymentRule'] = $this->setValidateData();

		$this->dataSetForStateCountryFunctions();

		// Update the templates path
		$app->config('templates.path', ROOT_DIR . '/routes/payments/' . $this->folder);

		// Return payment form
		echo $app->render('idPaymentTyEditForm.html', $this->data);
	}

	public function idPaymentTyPut_post($app) {

		$isIdPaymentPutSucceed = false;
		$msg = $this->data['requestType'];//"Request could not be accepted.";
		$errorCode = "";

		// retrieve Web text items
		$webText['webTextItems'] = retrieveWebTextAll(self::URL_CONDITION_WEB_TEXT, $this->data['language'], $this->data['PID']);

		if (isset($this->data['requestType'])) {
			// delete button is clicked on
			if ($this->data['requestType'] == "delete") {
				$this->execIDPaymentTy_del();
				$isIdPaymentPutSucceed = true;
				$msg = $this->setIDPaymentTyResultMessage($this->data['requestType']);
			}
			else if ($this->data['requestType'] == "createCard") {
				$rs = $this->execStripeCardCreate(
					$this->data['PID_1167_1'], //$exp_month
					$this->data['PID_1167_2'], //$exp_year
					$this->data['PID_1166'], //$cardnumber
					$this->data['PID_1168'], //$cvc
					$this->data['PID_1165'], //$cardname
					$this->data['PID_1171'], //$address_city
					$this->data['PID_1174'], //$address_country
					$this->data['PID_1169'], //$address_line1
					$this->data['PID_1170'], //$address_line2
					$this->data['PID_1172'], //$address_state
					$this->data['PID_1173'] //$address_zip
				);

				$isIdPaymentPutSucceed = ($rs['result'] == "0");
				$msg = $rs['message'];
				$errorCode = $rs['result'];
			}
			else if ($this->data['requestType'] == "updateCard") {
				$rs = $this->execStripeCardUpdate(
					$this->data['cardId'],
					$this->data['PID_1171'], //$address_city
					$this->data['PID_1174'], //$address_country
					$this->data['PID_1169'], //$address_line1
					$this->data['PID_1170'], //$address_line2
					$this->data['PID_1172'], //$address_state
					$this->data['PID_1173'], //$address_zip
					$this->data['PID_1167_1'], //$exp_month
					$this->data['PID_1167_2'], //$exp_year
					$this->data['PID_1165'] //$cardname
				);

				$isIdPaymentPutSucceed = ($rs['result'] == "0");
				$msg = $rs['message'];
				$errorCode = $rs['result'];
			}
			else if ($this->data['requestType'] == "deleteCard") {
				$rs = $this->execStripeCardDelete(
					$this->data['cardId']
				);

				$isIdPaymentPutSucceed = ($rs['result'] == "0");
				$msg = $rs['message'];
				$errorCode = $rs['result'];
			}
		}

		return $this->returnPaymentTransactionResult($isIdPaymentPutSucceed, (array)$msg, (array)$errorCode);
	}

	// Override of createPaymentFormElement() that relies on $this->data['cards']
	protected function createPaymentFormElement($idPaymentTyResult, $isSelectState = false, $card = NULL) {
		if(is_null($card)) {//Add new card
			$idNameGetRec = execIDName_get($this->data['id'],$this->data['userId']);
			if (ExecuteDB::existRecord($idNameGetRec)){
				$userCardHolder = $idNameGetRec[0]['Fname'].' '.$idNameGetRec[0]['Lname'];
			}
			else{
				$userCardHolder = '';
			}
			$monthSaved = '';
			$yearSaved = '';

			$addressRs = execIDAddress_get($this->data['id'],1,$this->data['language']);
			$userState = $addressRs['State'];
			$userCountry = $addressRs['Country'];
			$userAddress1 = $addressRs['Address1'];
			$userAddress2 = $addressRs['Address2'];
			$userCity = $addressRs['City'];
			$userZip = $addressRs['Zip'];
		}
		else {
			$userCardHolder = $card['name'];
			$monthSaved = $card['exp_month'];
			$yearSaved = substr($card['exp_year'], -2);
			$userState = $card['address_state'];
			$userCountry = $card['address_country'];
			$userAddress1 = $card['address_line1'];
			$userAddress2 = $card['address_line2'];
			$userCity = $card['address_city'];
			$userZip = $card['address_zip'];
		}

		foreach($idPaymentTyResult as $searchData) {
			$stateResult = execState_get($this->data['language'], $userCountry);

			$formElements = array();

			foreach ($idPaymentTyResult as $el) {
				$temp = array(
					'fieldLabel' => $el['Descr'], // label text
					'fieldName' => $el['PaymentValueParamID'], // PID_xxxx xxxx is from PaymentValueParamID (4 digit number)
					'fieldValueTy' => $el['PaymentValueTy'],
					'fieldOptions' => array(), // store option tag's value and display text
					'fieldType' => 'input', // html tag, By default is input, or could be overwritten
					'fieldID' => '', // ID attribute in input(type=text) tag
					'fieldLength' => $el['Length'] // length attribute in input(type=text) tag
				);

				switch($el['PaymentValueTy']) {
					// CardHolder Name
					case 35:
						// if not exist, set name (retrieved from ID info) to card holder name
						$temp['fieldValue'] = $userCardHolder;
						break;
					// Card Number
					case 36:
					// CVV Number
					case 38:
                        if(is_null($card)) {//Add new card
                            $temp['fieldValue'] = '';
                        }
                        else {
                            unset($temp);
                        }
						break;
					// CardHolder Name
					case 35:
						// if not exist, set name (retrieved from ID info) to card holder name
						$temp['fieldValue'] = $userCardHolder;
						break;
					// Card Expiry
					case 37:
						$temp['fieldType'] = 'select_expiry';

						// retrieve month name from webtext
						$monthWebText = retrieveWebTextAll('month', $this->data['language'], 0);
						$monthOptions = array();
						$monthMMtxt = retrieveWebTextSingle(self::URL_CONDITION_WEB_TEXT, $this->data['language'], $this->data['PID'], 131); // text is "mm"
						// set "mm" to option tag
						$monthOptions['value'] = "";
						$monthOptions['text'] = $monthMMtxt;
						$temp['fieldOptions'][0][] = $monthOptions;
						$monthValue = 1;
						$monthTextSuffix = ($this->data['language']==2)? $monthMMtxt : "";

						for ($textNumIndex = 4; $textNumIndex < 16; $textNumIndex++) { // textnum of month name (Jan): starts from 4
							$monthOptions['value'] = sprintf("%02d", $monthValue++);
							$monthOptions['text'] = $monthWebText[$textNumIndex].$monthTextSuffix;

							if ($monthOptions['value']==$monthSaved) {
								$monthOptions['selected'] = "selected";
							}
							else{
								$monthOptions['selected'] = "";
							}

							$temp['fieldOptions'][0][] = $monthOptions;
						}

						$yearOptions = array();
						// set "yy" to option tag
						$yearOptions['value'] = "";
						$yearOptions['text'] = retrieveWebTextSingle(self::URL_CONDITION_WEB_TEXT, $this->data['language'], $this->data['PID'], 132); // text is "yy"
						$temp['fieldOptions'][1][] = $yearOptions;

						$now = new DateTime();
						$yearValue = $now->format("Y") -1; // year: starts from previous year

						for ($textNumIndex = 0; $textNumIndex < 22; $textNumIndex++ ) {
							$yearOptions['value'] = substr($yearValue, 2, 2);
							$yearOptions['text'] = $yearValue++;

							if ($yearOptions['value']==$yearSaved) {
								$yearOptions['selected'] = "selected";
							}
							else{
								$yearOptions['selected'] = "";
							}

							$temp['fieldOptions'][1][] = $yearOptions;
						}
						break;
					// Billing Address
					case 39:
						$temp['fieldValue'] = $userAddress1;
						break;
					// Address2
					case 4:
						$temp['fieldValue'] = $userAddress2;
						break;
					// City
					case 8:
						$temp['fieldValue'] = $userCity;
						break;
					// State, need to pull up the states for the given country
					case 10:
						if ($isSelectState==true){
							$temp['fieldType'] = 'stateselect';
							$temp['fieldValue'] = $userState;
						}
						else {
							$temp['fieldType'] = 'select';
							$stateOption = array();

							foreach ($stateResult as $state) {
								$stateOption['value'] = $state['StateCode'];
								$stateOption['text'] = $state['StateCode']."(".$state['Descr'].")";

								if ($state['StateCode']==$userState) {
									$stateOption['selected'] = "selected";
								}
								else{
									$stateOption['selected'] = "";
								}

								$temp['fieldOptions'][] = $stateOption;
							}
						}
						break;
					// Zip
					case 12:
						// check whether zip exists in user's IDPaymentTy or not
						// if not exist, set city retrieved from ID info
						$temp['fieldValue'] = $userZip;
						break;
					//Country, need to display the countries in a list and default select
					case 13:
						$temp['fieldType'] = 'select';
						// get country item
						$countryResult = execCountry_get($this->data['language']);
						$countryOption = array();

						// if data not exist in IDPaymentTy, display Country of address from IDAddress record
						$temp['fieldValue'] = $userCountry;

						foreach ($countryResult as $country) {
							$countryOption['value'] = $country['country'];
							$countryOption['text'] = $country['country']."(".$country['Descr'].")";

							if ($country['country']==$temp['fieldValue']) {
								$countryOption['selected'] = "selected";
							}
							else {
								$countryOption['selected'] = "";
							}

							$temp['fieldOptions'][] = $countryOption;
						}
						break;
					default:
						unset($temp);
						break;
				} // end of switch

				if (isset($temp)) {
					$formElements[] = $temp;
				}
			}
		} // end of foreach loop

		return $formElements;
	}

	private function execStripeCharge() {

		$returnResult = array();

		$sql = 'EXEC dbo.StripeCharge '
			.'@ID=:id, '
			.'@OrderNum =:orderNum, '
			.'@UserID=:userId, '
			.'@IP=:ip';
		$parameterArry = array(
			':id' => $this->data['id'],
			':orderNum' => $this->data['orderNum'],
			':userId' => (isset($this->data['userId'])) ? $this->data['userId'] : '_none',
			':ip' => (isset($this->data['ip'])) ? $this->data['ip'] : '0.0.0.0'
		);

		Debug::loggingToTbl('StripeCharge' ,$parameterArry);
		$resultRec= ExecuteDB::executeSp($sql, $parameterArry, 1);
		Debug::loggingToTbl('StripeCharge result', $resultRec);

		$returnResult[0] = $resultRec['RefNum'];
		$returnResult[1] = $resultRec['RESULT']; // return nvarchar from stored procedure
		$returnResult[2] = $resultRec['RESPMSG'];
		$returnResult[3] = $resultRec['PNREF']; // return nvarchar from stored procedure

		return $returnResult;
	}

	private function execStripeCustomerRetrieve() {

		$sql = 'EXEC dbo.StripeCustomerRetrieve '
			.'@ID=:id, '
			.'@IDPaymentTyID=:idPaymentTyId, '
			.'@UserID=:userId';
		$parameterArry = array(
			':id' => $this->data['id'],
			':idPaymentTyId' => (isset($this->data['idPaymentTyId'])) ? $this->data['idPaymentTyId'] : NULL,
			':userId' => (isset($this->data['userId'])) ? $this->data['userId'] : '_none'
		);

		Debug::loggingToTbl('StripeCustomerRetrieve' ,$parameterArry);
		$resultRec = ExecuteDB::executeSp($sql, $parameterArry, 0);
		Debug::loggingToTbl('StripeCustomerRetrieve result', $resultRec);

		//TODO: need proper parsing of result
		return $resultRec;
	}

	private function execStripeCustomerCreate() {

		$sql = 'EXEC dbo.StripeCustomerCreate '
			.'@ID=:id, '
			.'@stripeToken=:stripeToken, '
			.'@IDPaymentTyID=:idPaymentTyId, '
			.'@UserID=:userId';
		$parameterArry = array(
			':id' => $this->data['id'],
			':stripeToken' => $this->data['stripeToken'],
			':idPaymentTyId' => (isset($this->data['idPaymentTyId'])) ? $this->data['idPaymentTyId'] : NULL,
			':userId' => (isset($this->data['userId'])) ? $this->data['userId'] : '_none'
		);

		Debug::loggingToTbl('execStripeCustomerCreate' ,$parameterArry);
		$resultRec = ExecuteDB::executeSp($sql, $parameterArry, 1);
		Debug::loggingToTbl('execStripeCustomerCreate result', $resultRec);

		//TODO: need proper parsing of result
		return $resultRec;
	}

	private function execStripeCustomerDelete($cardId) {

		$sql = 'EXEC dbo.StripeCustomerDelete '
			.'@ID=:id, '
			.'@IDPaymentTyID=:idPaymentTyId, '
			.'@UserID=:userId';
		$parameterArry = array(
			':id' => $this->data['id'],
			':idPaymentTyId' => (isset($this->data['idPaymentTyId'])) ? $this->data['idPaymentTyId'] : NULL,
			':userId' => (isset($this->data['userId'])) ? $this->data['userId'] : '_none'
		);

		Debug::loggingToTbl('execStripeCustomerDelete' ,$parameterArry);
		$resultRec = ExecuteDB::executeSp($sql, $parameterArry, 1);
		Debug::loggingToTbl('execStripeCustomerDelete result', $resultRec);

		//TODO: need proper parsing of result
		return $resultRec;
	}

	private function execStripeCardCreate($exp_month, $exp_year, $cardnumber, $cvc, $cardname, $address_city, $address_country, $address_line1, $address_line2, $address_state, $address_zip) {

		$sql = 'EXEC dbo.StripeCardCreate '
			.'@ID=:id, '
			.'@IDPaymentTyID=:idPaymentTyId, '
			.'@exp_month=:exp_month, '
			.'@exp_year=:exp_year, '
			.'@number=:cardnumber, '
			.'@cvc=:cvc, '
			.'@cardname=:cardname, '
			.'@address_city=:address_city, '
			.'@address_country=:address_country, '
			.'@address_line1=:address_line1, '
			.'@address_line2=:address_line2, '
			.'@address_state=:address_state, '
			.'@address_zip=:address_zip, '
			.'@UserID=:userId';
		$parameterArry = array(
			':id' => $this->data['id'],
			':idPaymentTyId' => (isset($this->data['idPaymentTyId'])) ? $this->data['idPaymentTyId'] : NULL,
			':exp_month' => $exp_month,
			':exp_year' => $exp_year,
			':cardnumber' => $cardnumber,
			':cvc' => $cvc,
			':cardname' => $cardname,
			':address_city' => $address_city,
			':address_country' => $address_country,
			':address_line1' => $address_line1,
			':address_line2' => $address_line2,
			':address_state' => $address_state,
			':address_zip' => $address_zip,
			':userId' => (isset($this->data['userId'])) ? $this->data['userId'] : '_none'
		);

		Debug::loggingToTbl('execStripeCardCreate',$parameterArry);
		$resultRec = ExecuteDB::executeSp($sql, $parameterArry, 1);
		Debug::loggingToTbl('execStripeCardCreate result', $resultRec);

		//TODO: need proper parsing of result
		return $resultRec;
	}

	private function execStripeCardUpdate($cardId, $address_city, $address_country, $address_line1, $address_line2, $address_state, $address_zip, $exp_month, $exp_year, $cardname) {

		$sql = 'EXEC dbo.StripeCardUpdate '
			.'@ID=:id, '
			.'@IDPaymentTyID=:idPaymentTyId, '
			.'@cardId=:cardId, '
			.'@address_city=:address_city, '
			.'@address_country=:address_country, '
			.'@address_line1=:address_line1, '
			.'@address_line2=:address_line2, '
			.'@address_state=:address_state, '
			.'@address_zip=:address_zip, '
			.'@exp_month=:exp_month, '
			.'@exp_year=:exp_year, '
			.'@cardname=:cardname, '
			.'@UserID=:userId';
		$parameterArry = array(
			':id' => $this->data['id'],
			':idPaymentTyId' => (isset($this->data['idPaymentTyId'])) ? $this->data['idPaymentTyId'] : NULL,
			':cardId' => $cardId,
			':address_city' => $address_city,
			':address_country' => $address_country,
			':address_line1' => $address_line1,
			':address_line2' => $address_line2,
			':address_state' => $address_state,
			':address_zip' => $address_zip,
			':exp_month' => $exp_month,
			':exp_year' => $exp_year,
			':cardname' => $cardname,
			':userId' => (isset($this->data['userId'])) ? $this->data['userId'] : '_none'
		);

		Debug::loggingToTbl('execStripeCardUpdate' ,$parameterArry);
		$resultRec = ExecuteDB::executeSp($sql, $parameterArry, 1);
		Debug::loggingToTbl('execStripeCardUpdate result', $resultRec);

		//TODO: need proper parsing of result
		return $resultRec;
	}

	private function execStripeCardDelete($cardId) {

		$sql = 'EXEC dbo.StripeCardDelete '
			.'@ID=:id, '
			.'@IDPaymentTyID=:idPaymentTyId, '
			.'@cardId=:cardId, '
			.'@UserID=:userId';
		$parameterArry = array(
			':id' => $this->data['id'],
			':idPaymentTyId' => (isset($this->data['idPaymentTyId'])) ? $this->data['idPaymentTyId'] : NULL,
			':cardId' => $cardId,
			':userId' => (isset($this->data['userId'])) ? $this->data['userId'] : '_none'
		);

		Debug::loggingToTbl('execStripeCardDelete' ,$parameterArry);
		$resultRec = ExecuteDB::executeSp($sql, $parameterArry, 1);
		Debug::loggingToTbl('execStripeCardDelete result', $resultRec);

		//TODO: need proper parsing of result
		return $resultRec;
	}
}
